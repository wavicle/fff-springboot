Test URL (for user ID = 1):
http://localhost:8080/test.html?userId=1

Report URL:
http://localhost:8080/report.html

How to add new questions:
Add a new YAML object in c:/fff/challenges.yml. This file path can be changed in FFFApplication.java

Change logs:
1.0.0 - Initial version
2.0.0 - Ability to load challenges from YAML file
2.1.0 - Challenges appear randomly on the test page
2.2.0 - Ability to load challenges from the classpath