var app = angular.module('myApp', []);
var challenges;

/** This controller presents and accepts replies for challenges * */
app.controller('testController', function($scope, $http) {
    $scope.challengeIndex = -1;

    $http({
        method : "GET",
        url : "/challenges"
    }).then(function(response) {
        console.log("Challenges: ", response);
        challenges = response.data;
        $scope.nextChallenge();
    }, function(response) {
        console.log(response);
    });

    $scope.nextChallenge = function() {
        $scope.timerStart = (new Date()).getTime();
        $scope.challengeIndex++;
        if ($scope.challengeIndex >= challenges.length) {
            $scope.challengeIndex = 0;
            alert("Thank you! All challenges have been submitted. Resetting the test.");
        }
        $scope.challenge = challenges[$scope.challengeIndex];
    };

    $scope.chooseOption = function(index) {
        $scope.optionId = $scope.challenge.options[index].id;
    };

    $scope.submit = function() {
        $scope.timerEnd = (new Date()).getTime();
        var timeConsumed = $scope.timerEnd - $scope.timerStart;
        var submission = {
            challengeId : $scope.challenge.id,
            optionId : $scope.optionId,
            userId : userId,
            timeConsumed : timeConsumed
        };
        console.log('Submitting user reply', submission);
        $http({
            method : 'POST',
            url : '/submit',
            headers : {
                'Content-Type' : 'application/json'
            },
            data : submission
        }).then(function(response) {
            console.log('Submitted successfully!');
        }, function(response) {
            console.log('Submission failed:', response);
        });
    };
});

/** This controller controls the test reports * */
app.controller('reportController', function($scope, $http) {
    $http({
        method : 'POST',
        url : 'report',
        headers : {
            'Content-Type' : 'application/json'
        },
        data : {}
    }).then(function(response) {
        var report = response.data;
        console.log('Completed successfully!', report);

        /** Further, group this report by the challenge name * */
        var groupedReport = {};
        for ( var i in report) {
            var responseItem = report[i];
            var challengeId = responseItem.challengeId;
            var groupedItems = groupedReport[challengeId] || [];
            groupedItems.push(responseItem);
            groupedReport[challengeId] = groupedItems;
        }
        console.log("Grouped Report", groupedReport);
        $scope.groupedReport = groupedReport;
    }, function(response) {
        console.log('Query failed:', response);
    });

});