package com.konecranes.fff;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import com.konecranes.fff.domain.Challenge;
import com.konecranes.fff.domain.User;

import wavicle.starters.hibernate.DataService;
import wavicle.starters.hibernate.HBM2DDLPolicy;
import wavicle.starters.hibernate.SimpleConfiguration;

@EnableAutoConfiguration
@ComponentScan("com.konecranes.fff")
public class FFFApplication {

	private static Logger LOG = LoggerFactory.getLogger(FFFApplication.class);

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(FFFApplication.class, args);
	}

	private static DataService dataService;

	public FFFApplication() {
		dataService = new DataService();
		dataService.prepare(new SimpleConfiguration() {
			@Override
			public List<String> getPackagesToScan() {
				return Arrays.asList("com.konecranes.fff.domain");
			}

			@Override
			public String getJdbcUsername() {
				return "sa";
			}

			@Override
			public String getJdbcUrl() {
				return "jdbc:hsqldb:mem:fffdb";
			}

			@Override
			public String getJdbcPassword() {
				return "";
			}

			@Override
			public String getJdbcDriverClass() {
				return "org.hsqldb.jdbcDriver";
			}

			@Override
			public HBM2DDLPolicy getHBM2DDLPolicy() {
				return HBM2DDLPolicy.UPDATE;
			}
		});

		setupData();

	}

	private void setupData() {
		try {
			User user1 = new User("Customer", "1");
			User user2 = new User("Customer", "2");
			User user3 = new User("Customer", "3");

			dataService.save(user1);
			dataService.save(user2);
			dataService.save(user3);

			List<Challenge> challenges = CommonUtils.loadFromResource("data/challenges.yml");
			challenges.forEach(c -> dataService.save(c));
		} catch (Exception e) {
			LOG.error("Error occurred while load challenges", e);
		}
	}

	public static DataService getDataService() {
		return dataService;
	}

}
