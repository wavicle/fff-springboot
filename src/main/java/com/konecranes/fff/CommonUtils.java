package com.konecranes.fff;

import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.konecranes.fff.domain.Challenge;
import com.konecranes.fff.domain.Option;

/**
 * Common Utils for this application.
 *
 * @author wavicle
 */
public class CommonUtils {

	/**
	 * Load challenges from resource.
	 *
	 * @param path
	 *            the file path
	 * @return the list
	 */
	public static List<Challenge> loadFromResource(String path) {
		try {
			InputStream is = CommonUtils.class.getClassLoader().getResourceAsStream(path);
			Reader reader = new InputStreamReader(is);
			return loadFromReader(reader);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Load challenges from file.
	 *
	 * @param filePath
	 *            the file path
	 * @return the list
	 */
	public static List<Challenge> loadFromFile(String filePath) {
		File file = new File(filePath);
		try {
			FileReader reader = new FileReader(file);
			return loadFromReader(reader);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Load challenges from reader.
	 *
	 * @param reader
	 *            the reader
	 * @return the list
	 * @throws YamlException
	 *             the yaml exception
	 */
	private static List<Challenge> loadFromReader(Reader reader) throws YamlException {
		List<Challenge> challenges = new ArrayList<>();
		YamlReader yamlReader = new YamlReader(reader);
		YamlConfig config = yamlReader.getConfig();
		config.setClassTag("challenge", Challenge.class);
		config.setClassTag("option", Option.class);
		while (true) {
			Challenge challenge = yamlReader.read(Challenge.class);
			if (challenge != null) {
				challenges.add(challenge);
			} else {
				break;
			}
		}
		return challenges;
	}
}
