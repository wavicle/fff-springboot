package com.konecranes.fff.ctrl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.konecranes.fff.FFFApplication;
import com.konecranes.fff.domain.Challenge;
import com.konecranes.fff.domain.SuccessfulAttempt;
import com.konecranes.fff.domain.UserSubmission;

@Controller
public class FffRestController {

	private static Logger LOG = LoggerFactory.getLogger(FffRestController.class);

	@RequestMapping("/test.html")
	public ModelAndView test(HttpServletRequest request) {
		String userId = request.getParameter("userId");
		LOG.info("Received test request for user ID: {}", userId);
		ModelAndView mv = new ModelAndView("test");
		mv.addObject("userId", userId);
		return mv;
	}

	@RequestMapping("/challenges")
	public @ResponseBody Object challenges() {
		List<Challenge> challenges = FFFApplication.getDataService().query("FROM Challenge c", Collections.emptyMap(),
				Challenge.class, -1);
		Collections.shuffle(challenges, new Random(System.currentTimeMillis()));
		return challenges;
	}

	@RequestMapping("/submit")
	public @ResponseBody Object submit(@RequestBody UserSubmission submission) {
		LOG.info("Received submission: {}", ToStringBuilder.reflectionToString(submission));
		synchronized (this) {
			FFFApplication.getDataService().doInTransaction((session, tx) -> {
				Map<String, Object> params = new HashMap<>();
				params.put("userId", submission.getUserId());
				params.put("challengeId", submission.getChallengeId());
				Query<?> delete = session.createQuery(
						"DELETE FROM UserSubmission WHERE userId = :userId AND challengeId = :challengeId");
				delete.setParameter("challengeId", submission.getChallengeId());
				delete.setParameter("userId", submission.getUserId());
				delete.executeUpdate();
				session.save(submission);
			});
		}
		return Collections.singletonMap("result", "ok");
	}

	@RequestMapping("/report.html")
	public String reportHtml() {
		return "report";
	}

	@RequestMapping("/report")
	public @ResponseBody Object report(Map<String, String> params) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("  select                          ");
		sb.append("    sub.id as id,                 ");
		sb.append("    c.content as challenge,       ");
		sb.append("    c.id as challengeId,          ");
		sb.append("    opt.content as option,        ");
		sb.append("    u.firstname,                  ");
		sb.append("    u.lastname,                   ");
		sb.append("    sub.timeconsumed              ");
		sb.append("  from                            ");
		sb.append("    challenge c,                  ");
		sb.append("    challenge_option ch_opt,      ");
		sb.append("    option opt,                   ");
		sb.append("    usersubmission sub,           ");
		sb.append("    user u                        ");
		sb.append("  where c.id = ch_opt.challenge_id");
		sb.append("  and opt.id = ch_opt.options_id  ");
		sb.append("  and opt.correct = true          ");
		sb.append("  and sub.challengeid = c.id      ");
		sb.append("  and sub.optionid = opt.id       ");
		sb.append("  and u.id = sub.userid           ");
		sb.append("  order by c.id, sub.timeconsumed ");
		String sql = sb.toString();
		List<SuccessfulAttempt> results = FFFApplication.getDataService().sqlQuery(sql, Collections.emptyMap(),
				SuccessfulAttempt.class, -1);
		return results;
	}

}